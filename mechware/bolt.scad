module bolt_model(
    screw_dia,
    screw_len,
    
    head_dia,
    head_len,
){
    union(){
        /* screw */
        translate([0, 0, -$fix])
        cylinder(r=screw_dia/2, h=screw_len+$fix);
        
        /* head */
        translate([0, 0, -head_len])
        cylinder(r=head_dia/2, h=head_len);
    }
}

M2 = [
    2.0,
    3.5,
    1.2,
];

M3 = [
    3.0,
    5.0,
    2.0,
];

M4 = [
    4.0,
    8.0,
    3.5,
];

M5 = [
    5.0,
    
];

M8 = [
    8.0,
    15.0,
    7.0,
];

module bolt(
    std=M3,
    len=6.0,
    
    screw_out=2.0,
    head_out=2,
){
    screw_dia=std[0];
    screw_len=len;
    head_dia=std[1];
    head_len=std[2];
    
    #bolt_model(screw_dia, screw_len, head_dia, head_len);
    bolt_model(screw_dia+$clr*2, screw_len+screw_out, head_dia+$clr*2, head_len+head_out);
}

module nut_model(
    screw_dia,
    head_dia,
    len,
){
    difference(){
        cylinder(r=head_dia/2, h=len, $fn=6);

        translate([0, 0, -$fix])
        cylinder(r=screw_dia/2, h=len+$fix*2);
    }
}

module nut(
    std=M3,
){
    screw_dia=std[0];
    head_dia=std[1];
    len=std[2];
    
    #nut_model(screw_dia, head_dia, len);
    nut_model(screw_dia-$clr*2, head_dia+$clr*2, len);
}

module horn_nut(
    std=M3
){
    screw_dia=std[0];
    head_dia=std[1];
    len=std[2];
    
    horn_dia=head_dia*2;
    horn_width=len;
    horn_thickness=head_dia/2-screw_dia/2;
    horn_len=horn_dia/2*sqrt(2);
    
    difference(){
        union(){
            cylinder(r=head_dia/2, h=len);
            
            for(m=[0, 1])
            mirror([m, 0, 0])
            translate([screw_dia/2, 0, len/2])
            rotate([0, 45, 0])
            translate([-horn_thickness/2, -horn_width/2, horn_thickness/2])
            cube([horn_thickness, horn_width, horn_len]);
        }
        
        translate([0, 0, -$fix])
        cylinder(r=screw_dia/2, h=len+$fix*2);
    }
}
