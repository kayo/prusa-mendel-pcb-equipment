mount_distance=209;

bolt_depth=1;

nut_width=20;
nut_depth=8;
nut_offset=8;

rod_diameter=8;
rod_top=5;
rod_offset=1;
rod_bottom=6;
rod_length=mount_distance-nut_offset*2;

holder_border=2;
holder_depth=6;
holder_height=6;

border_thickness=6;
wall_thickness=10;
bottom_height=6;

bottom_slice=bottom_height+rod_bottom-rod_top;

rod_bottom_height=bottom_height+rod_bottom+rod_diameter/2;
rod_top_height=rod_bottom_height+rod_diameter+rod_offset;

echo("rod_bottom_height:", rod_bottom_height);
echo("rod_top_height:", rod_top_height);

width=rod_diameter/2+wall_thickness;
length=nut_offset+nut_depth+border_thickness;
height=bottom_height+rod_bottom+rod_diameter*2+rod_offset+rod_top;

border_distance=mount_distance-(nut_offset+nut_depth+border_thickness)*2;

module holes_ring(
    height,
    diameter=5,
    offset=width/2,
    count=6,
){
    for(i=[0:count-1])
    rotate([0, 0, i*360/count])
    translate([offset, 0, 0])
    cylinder(r=diameter/2, h=height);
}

module frame(){
    for(x=[-mount_distance/2, 0, mount_distance/2])
    translate([x, 0, rod_bottom_height]){
        translate([0, rod_length/2])
        rotate([90, 0, 0])
        cylinder(r=rod_diameter/2, h=rod_length);
        
        for(m1=[0, x?1:0], m2=[0, 1])
        mirror([0, m1, 0])
        translate([0, -border_distance/2-border_thickness/2, 0])
        mirror([0, m2, 0])
        translate([0, border_thickness/2, 0])
        rotate([-90, 0, 0])
        nut(M8);

        translate([0, border_thickness+nut_width/2, 0])
        rotate([-90, 0, 0])
        rotate([0, 0, 90])
        horn_nut(M8);
    }
    
    for(y=[0, -mount_distance/2])
    translate([0, y, rod_top_height]){
        translate([-rod_length/2, 0])
        rotate([0, 90, 0])
        cylinder(r=rod_diameter/2, h=rod_length);
        
        for(m1=[0, 1], m2=[0, 1])
        mirror([m1, 0, 0])
        translate([border_distance/2+border_thickness/2, 0, 0])
        mirror([m2, 0, 0])
        translate([border_thickness/2, 0, 0])
        rotate([0, 90, 0])
        nut(M8);
        
        translate([border_thickness+nut_width/2, 0, 0])
        rotate([0, 90, 0])
        horn_nut(M8);
    }
}

include <bolt.scad>

module corner(
    type, /* a|b|c|d */
){
    mirror([type=="b"||type=="d"?1:0, 0])
    mirror([0, type=="c"||type=="d"?1:0])
    difference(){
        union(){
            if(type=="c"||type=="d"){
                translate([-width, 0, 0])
                cube([width*2, length, height]);
            }else{
                translate([-width, 0, 0])
                cube([width+length, length, height]);
                
                translate([0, -width, 0])
                cube([length, width+length, height]);
            }
            
            cylinder(r=width, h=bottom_height);
        }

        /* holes */
        *translate([0, 0, -$fix])
        rotate([0, 0, 30])
        holes_ring(offset=(width+M3[1]/2)/2, height=bottom_height+$fix*2);
        
        /* window */
        if(type=="c"||type=="d"){
            translate([-width-$fix, -width-$fix, bottom_height])
            cube([width+nut_width/2+$fix, width+nut_offset+nut_depth+$fix, height-bottom_height+$fix]);
        }else{
            translate([-width-$fix, -width-$fix, bottom_height]){
                cube([width+nut_width/2+$fix, width+nut_offset+nut_depth+$fix, height-bottom_height+$fix]);
                cube([width+nut_offset+nut_depth+$fix, width+nut_width/2+$fix, height-bottom_height+$fix]);
            }
        }

        /* holder */
        translate([length-holder_depth, length-holder_depth, -$fix]){
            cube([holder_depth+$fix, holder_depth+$fix, height+$fix*2]);
            
            if(type=="a"){
                //translate([-holder_border, -holder_border, height-holder_height+$fix])
                //cube([holder_depth+holder_border+$fix, holder_depth+holder_border+$fix, height+$fix*2]);
                
                translate([holder_depth, holder_depth, height-holder_height+$fix])
                rotate([0, 0, 45])
                cylinder(r1=(holder_depth+holder_border)*sqrt(2), r2=(holder_depth+holder_border/2)*sqrt(2), h=holder_height+$fix, $fn=4);

                translate([-holder_border/2, -holder_border/2, height-holder_height+$fix])
                rotate([0, 0, 45])
                cylinder(r=holder_border, h=holder_height+$fix, $fn=4);
            }
        }

        /* bottom rod */
        translate([0, nut_offset+nut_depth-$fix, rod_bottom_height])
        rotate([-90, 0, 0])
        cylinder(r=rod_diameter/2+$clr, h=border_thickness+$fix*2);
        
        if(type!="c"){
            /* top rod */
            translate([nut_offset+nut_depth-$fix, 0, rod_top_height])
            rotate([0, 90, 0])
            cylinder(r=rod_diameter/2+$clr, h=border_thickness+$fix*2);
        }
        
        translate([0, 0, bottom_height-bolt_depth])
        mirror([0, 0, 1])
        bolt(M3, bottom_height);
    }
}

module slider(
    type, /* a|b|c|d */
){
    width2=nut_width/2+border_thickness;
    nut_depth2=length-width2;
    
    mirror([type=="b"?1:0, 0])
    mirror([0, type=="c"?1:0])
    difference(){
        translate([-length, -(type=="d"?width2:length), bottom_slice+$fix])
        cube([(type=="b"||type=="d"?width:width2)+length, (type=="c"?width:width2)+(type=="d"?width2:length), height-bottom_slice-$fix]);
        
        /* window */
        if(type=="a"){
            translate([-nut_width/2, -nut_width/2, bottom_height])
            cube([nut_width, nut_width, height-bottom_height+$fix]);
        }else if(type=="c"){
            translate([-nut_width/2, -length+border_thickness, bottom_height])
            cube([nut_width, nut_width/2+nut_offset+nut_depth, height-bottom_height+$fix]);
        }else{
            translate([-length+border_thickness, -nut_width/2, bottom_height])
            cube([nut_width/2+nut_offset+nut_depth, nut_width, height-bottom_height+$fix]);
        }
        
        /* holder */
        for(mx=[0, 1], my=[0, 1])
        mirror([mx, 0, 0])
        mirror([0, my, 0])
        translate([-length-$fix, -length-$fix, -$fix]){
            cube([holder_depth+$fix, holder_depth+$fix, height+$fix*2]);
            
            if(type!="d" && mx==0 && my==0){
                //translate([0, 0, height-holder_height+$fix])
                //cube([holder_depth+holder_border+$fix, holder_depth+holder_border+$fix, height+$fix*2]);
                
                translate([0, 0, height-holder_height+$fix])
                rotate([0, 0, 45])
                cylinder(r1=(holder_depth+holder_border)*sqrt(2), r2=(holder_depth+holder_border/2)*sqrt(2), h=holder_height+$fix, $fn=4);
                
                translate([holder_depth+holder_border/2, holder_depth+holder_border/2, height-holder_height+$fix])
                rotate([0, 0, 45])
                cylinder(r=holder_border, h=holder_height+$fix, $fn=4);
            }
        }
        
        /* nut window */
        if(type=="a"||type=="c")
        //for(mx=[0, 1])
        //mirror([mx, 0, 0])
        translate([-length-$fix, -nut_width/2, -$fix])
        cube([nut_depth2+$fix, nut_width+border_thickness+$fix, height+$fix*2]);
        
        if(type=="a"||type=="b"||type=="d")
        //for(my=[0, 1])
        //mirror([0, my, 0])
        translate([-nut_width/2, -length-$fix, -$fix])
        cube([nut_width+border_thickness+$fix, nut_depth2+$fix, height+$fix*2]);
        
        /* bottom rod */
        translate([0, -length-$fix, rod_bottom_height])
        rotate([-90, 0, 0])
        cylinder(r=rod_diameter/2+$clr, h=length*2+$fix*2);
        
        /* top rod */
        translate([-length-$fix, 0, rod_top_height])
        rotate([0, 90, 0])
        cylinder(r=rod_diameter/2+$clr, h=length*2+$fix*2);
    }
}

module holder(print=false){
    distance=print?mount_distance/3.5:mount_distance/2;
    offset=print?-bottom_slice:0;
    
    if(!print){
        %frame();
    }
    
    translate([-distance, -distance])
    corner(type="a");
    
    translate([distance, -distance])
    corner(type="b");

    translate([-distance, distance])
    corner(type="c");

    translate([distance, distance])
    corner(type="d");
    
    translate([0, 0, offset]){
        translate([0, 0])
        slider(type="a");
        
        translate([-distance, 0])
        slider(type="b");
        
        translate([0, -distance])
        slider(type="c");
        
        translate([distance, 0])
        slider(type="d");
    }
}

$fn=100;
$fix=0.01;
$clr=0.2;

holder(print=true);
