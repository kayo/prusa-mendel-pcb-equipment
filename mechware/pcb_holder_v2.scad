use <../extern/MCAD/nuts_and_bolts.scad>

module part(
  top_thickness=3.5,
  bottom_thickness=3.5,
  edge_thickness=1.6,
  side_square=1.8,
  extra_offset=4.4,
  
  rod_diameter=6,
  rod_distance=1,
  nut_diameter=10,

  rod=[true, true],
  nut=[true, true],
  
  board=true,
  board_width=2,
  board_height=2,
  board_hold=0.5,
  board_depth=7,
  
  holder=true,
  holder_width=12,
  holder_bolt=3,
  holder_head=7,
  holder_thickness=4
){
  nut_width=nut_diameter*sqrt(3)/2;
  side_thickness=edge_thickness*2+side_square;
  width=nut_width+side_thickness*2+extra_offset; //(nut[0]&&nut[1]?extra_offset:0);
  height=top_thickness+rod_diameter*2+rod_distance+bottom_thickness;

  rodless = nut_width-side_square-edge_thickness;
  
  intr_width=width-edge_thickness*2-side_square;
  intr_thickness=edge_thickness*3;
  n_squares=floor(intr_width/(side_square+intr_thickness));
  square_distance=intr_width/n_squares;
  
  holder_offset=-width/2+edge_thickness+side_square/2;
  
  board_offset=-width/2+edge_thickness+side_square/2;
  board_bottom=edge_thickness+side_square/2;
  board_top=board_bottom+board_hold;
  board_holder=board_height+board_depth;
  
  difference(){
    union(){
      /* body cube */
      translate([-width/2, -width/2, 0])
      cube([width-(rod[1]?0:rodless), width-(rod[0]?0:rodless), height]);
      
      /* holder anchor */
      if(holder || board)
      translate([holder_offset, holder_offset, 0]){
        translate([-holder_width/2, -holder_width/2, 0])
        cube([holder_width, holder_width, /*holder_thickness*/height]);
        *cylinder(r=holder_width/2, h=holder_thickness);
      }
      
      /* board holder */
      if(board)
      translate([board_offset, board_offset, 0])
      cube([board_width*2, height]);
    }
    
    union(){
      /* edge squares */
      /*
      for(mx=[0, 1], my=[0, 1])
      mirror([mx, 0, 0])
      mirror([0, my, 0])
      translate([-width/2+edge_thickness, -width/2+edge_thickness, -$fix])
      cube([side_square, side_square, height+$fix*2]);
      */
      for(i=[0:n_squares], j=[0:n_squares])
      translate([-width/2+edge_thickness+i*square_distance, -width/2+edge_thickness+j*square_distance, -$fix])
      cube([side_square, side_square, height+$fix*2]);
      
      /* rod's holes and nuts */
      for(tr=[[0, 0], [rod_diameter+rod_distance, 90]])
      rotate([0, 0, tr[1]])
      translate([0, 0, bottom_thickness+rod_diameter/2+tr[0]])
      rotate([0, 90, 0]){
        /* rod's hole */
        if(rod[0] && tr[0] == 0 || rod[1] && tr[0] != 0)
        translate([0, 0, width/2+$fix])
        mirror([0, 0, 1])
        boltHole(rod_diameter, length=width+board_width*2+$fix*2, tolerance=$clr*3);

        if(nut[0] && tr[0] == 0 || nut[1] && tr[0] != 0)
        translate([0, 0, width/2-edge_thickness])
        mirror([0, 0, 1]){
          /* rod's nut */
          #nutHole(rod_diameter, tolerance=$clr*2);
          
          /* nut's hole */
          rotate([0, 0, tr[1]*2])
          translate([rod_diameter-$clr, 0, 0])
          nutHole(rod_diameter, tolerance=$clr*2);
        }
      }
      
      /* part's holder bolt and head */
      translate([holder_offset, holder_offset, 0]){
        if(holder || board)
        translate([0, 0, holder_thickness])
        cylinder(r=holder_head/2, h=height);
        
        if(holder)
        translate([0, 0, holder_thickness+$fix])
        mirror([0, 0, 1])
        #boltHole(holder_bolt, length=holder_thickness+$fix*2, tolerance=$clr*2);
      }
      
      /* part's holder bolt */
      /*if(holder)
      translate([holder_offset, holder_offset, -$fix])
      cylinder(r=holder_bolt/2, h=holder_thickness+$fix*2);*/
      
      /* board's holder */
      if(board){
        translate([board_offset-board_top*2-board_width, board_offset-board_top*2-board_width, height-board_holder-$fix])
        cube([board_top*2, board_top*2, board_holder+$fix*2]);
        
        translate([board_offset-board_top-board_width, board_offset-board_top-board_width, height-board_height])
        rotate([0, 0, 45])
        cylinder(r2=(board_bottom+board_width)*sqrt(2), r1=(board_top+board_width)*sqrt(2), h=board_height+$fix, $fn=4);
      }
    }
  }
}

module butterflyNut(
  diameter=6,
  height=5,
  thickness=1
){
  bottom_diameter=diameter*sqrt(3);
  top_diameter=diameter+thickness*2;
  middle_diameter=(bottom_diameter+top_diameter)/2;
  
  cylinder(r1=bottom_diameter/2, r2=top_diameter/2, h=height);

  for(m=[0, 1])
  mirror([m, 0, 0])
  rotate([0, -45, 0])
  translate([height/2, -height/2, -middle_diameter/4])
  cube([diameter*2, height, height/2]);
}

module assembly(
  print=false
){
  $fn=print?24:100;
  $fix=0.01;
  $clr=0.2;
  
  distance=print?65:100;
  
  for(mx=[0, 1], my=[0, 1])
  mirror([mx, 0, 0])
  mirror([0, my, 0])
  translate([distance/2, distance/2, 0])
  if(mx!=my)
  part(board=false, nut=[false, false], rod=[!my, !mx]);
  
  translate([distance/4, distance/4, 0])
  for(mx=[0, 1], my=[0, 1])
  mirror([mx, 0, 0])
  mirror([0, my, 0])
  translate([distance/4, distance/4, 0])
  //part(holder=!(mx>0||my>0));
  part(holder=!mx&&!my, nut=[!!mx, !!my]);

  %for(tx=[0, distance/2], tz=[[13.5, 0], [6.5, 1]])
  mirror([tz[1], 0, 0])
  mirror([0, tz[1], 0])
  mirror([tz[1], tz[1], 0])
  translate([tx, distance/2+20, tz[0]])
  rotate([90, 0, 0]){
    cylinder(r=3, h=distance+35);
    
    for(tr=[[5, 0], [distance+22, -45], [distance+22+5, 45]])
    translate([0, 0, tr[0]])
    rotate([0, 0, tr[1]])
    if(tx || tr[0]==5)
    nutHole(6);

    translate([0, 0, 5])
    mirror([0, 0, 1])
    butterflyNut(6);
  }
}

assembly(
  //print=true
);
