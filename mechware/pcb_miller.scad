use <../extern/PrusaMendel/source/x-carriage.scad>
use <../extern/PrusaMendel/source/belt-clamp.scad>
include <bolt.scad>

module motor_body(
    motor_diameter=52,
    motor_length=85,

    shaft_diameter=8,
    shaft_length=20,
    
    caption_diameter=26,
    caption_length=6.5,
){
    union(){
        translate([0, 0, -$clr])
        cylinder(r=motor_diameter/2+$clr, h=motor_length+$clr*2);
        
        for(z=[-caption_length-$clr, motor_length-$fix+$clr])
        translate([0, 0, z])
        cylinder(r=caption_diameter/2+$clr, h=caption_length+$fix+$clr);
        
        translate([0, 0, -caption_length-shaft_length-$clr])
        cylinder(r=shaft_diameter/2+$clr, h=shaft_length+$fix+$clr);
    }
}

module motor_mount(
    motor_diameter=52,
    motor_length=85,

    shaft_diameter=8,
    shaft_length=20,
    
    caption_diameter=26,
    caption_length=6.5,
    
    mount_distance=35,
    mount_standart=M4,
    mount_length=5,
){
    motor_body(
        motor_diameter,
        motor_length,
        
        shaft_diameter,
        shaft_length,
        
        caption_diameter,
        caption_length
    );
    
    #motor_body(
        motor_diameter,
        motor_length,
        
        shaft_diameter,
        shaft_length,
        
        caption_diameter,
        caption_length,
        
        $clr=0
    );
    
    for(rz=[0, 90, 180, 270])
    rotate([0, 0, 45+rz])
    translate([mount_distance/2, 0, -mount_length])
    bolt(mount_standart, len=mount_length);
}

module adapter(
    length=80,
    width=60,
    motor_depth=3,
    thickness=10,
    mount_distance=50,
    mount_standart=M4,
){
    difference(){
        cylinder(r=width/2, h=thickness);
        
        translate([0, 0, thickness-motor_depth])
        motor_mount(caption_length=thickness-motor_depth+$fix);

        for(y=[-5, 17])
        translate([-39, y, 7+4])
        scale([1.05, 1.1, 1.6])
        mirror([0, 0, 1]){
            translate([9, 0, 0])
            cylinder(r=2, h=10);
            beltclamp();
        }
        
        for(m=[0, 1])
        mirror([0, m, 0])
        translate([0, mount_distance/2, 0]){
            translate([0, 0, thickness-motor_depth-$fix])
            cylinder(r=M4[1]/2+$clr, h=motor_depth+$fix*2);
            
            translate([0, 0, thickness-motor_depth-M4[2]])
            nut(M4);
            
            bolt(M4);
        }
    }
}

module design(
    print=false
){
    if(!print){
        %translate([0, 6, 0])
        rotate([0, 180, 0])
        render()
        xcarriage(true);
        
        %for(y=[-5, 17])
        translate([-39, y, 7])
        mirror([0, 0, 1])
        render()
        beltclamp();
    }
    
    adapter();
}

$fn=100;
$fix=0.01;
$clr=0.3;

design(
    //print=true
);
