module GCode ( Value(Value)
             , Entry(Entry)
             , Command(Command)
             , Instruction(Entries, Directive, Empty)
             , valueFrom
             , valueFrom'
             , valueTo
             , parse
             , format
             , formatValue
             ) where

import Data.Char(toUpper)

data Value =
  Value
  { intValue :: Int 
  , decPoint :: Int
  } deriving (Show, Eq)

data Entry = Entry Char Value
           deriving (Show, Eq)

data Instruction = Entries [Entry]
                 | Directive String String
                 | Empty
                 deriving (Show, Eq)

data Command = Command Instruction String
             deriving (Show, Eq)

parse = parseCommand
format = formatCommand

class (Num a) => Val a where
  valueFrom :: Int -> a -> Value
--  valueFrom p v = Value (v * 10 ^ p) p
  valueFrom' :: a -> Value
  valueFrom' = valueFrom 0
  valueTo :: Value -> a
--  valueTo (Value d p) = d / 10 ^ p

--instance (Integral a) => Val a where
instance Val Int where
  valueFrom p v = Value (v * 10 ^ p) p
  valueTo (Value d p) = d `div` 10 ^ p

--instance Floating a => Val a where
instance Val Double where
  valueFrom p v = Value (round (v * 10.0 ^^ p)) p
  valueTo (Value d p) = fromIntegral d / 10 ^ p

instance Val Float where
  valueFrom p v = Value (round (v * 10.0 ^^ p)) p
  valueTo (Value d p) = fromIntegral d / 10 ^ p

parseCommand :: String -> Command
parseCommand "" = Command Empty ""
parseCommand s = result [parseEntries s, parseDirective s, (Empty, s)]
  where
    -- result :: [(Instruction, String)] -> Command
    result [] = result' (Empty, "")
    result (n@((Empty), _):[]) = result' n
    result (e@((Entries _), _):_) = result' e
    result (d@((Directive _ _), _):_) = result' d
    result (_:r) = result r
    
    -- result' :: (Instruction, String) -> Command
    result' (i, s) = Command i $ parseComment s
    
parseComment :: String -> String
parseComment "" = ""
parseComment (' ':r) = parseComment r
parseComment (';':r) = commentContent r
  where
    -- commentContent :: String -> String
    commentContent "" = ""
    commentContent (' ':r) = commentContent r
    commentContent s = commentEnd s
    
    -- commentEnd :: String -> String
    commentEnd "" = ""
    commentEnd " " = ""
    commentEnd "\t" = ""
    commentEnd (s:[]) = [s]
    commentEnd (s:r) = let e = commentEnd r in if e == "" then commentEnd [s] else s:e
parseComment _ = ""

parseDirective :: String -> (Instruction, String)
parseDirective "" = (Empty, "")
parseDirective (' ':r) = parseDirective r
parseDirective s = result $ parseKey s
  where
    result :: (String, String, String) -> (Instruction, String)
    result ("", _, s) = (Empty, s)
    result (_, "", s) = (Empty, s)
    result (k, v, s) = (Directive k v, s)
    
    parseKey :: String -> (String, String, String)
    parseKey [] = ("", "", "")
    parseKey (',':r) = parseVal r
    parseKey (c:r) | c `elem` ['A' .. 'Z'] ++ ['a' .. 'z'] = resultKey c $ parseKey r
                   | otherwise = ("", "", r)
    
    resultKey c (k, v, s) = (c : k, v, s)
    
    parseVal :: String -> (String, String, String)
    parseVal [] = ("", "", "")
    parseVal (c:r) | c `elem` ['A' .. 'Z'] ++ ['a' .. 'z'] ++ ['0' .. '9'] = resultVal c $ parseVal r
                   | otherwise = ("", "", r)
    
    resultVal c (k, v, s) = (k, c : v, s)

parseEntries :: String -> (Instruction, String)
parseEntries s = result $ parseEntries' s
  where
    result :: ([Entry], String) -> (Instruction, String)
    result ([], s) = (Empty, s)
    result (e, s) = (Entries e, s)
    
    parseEntries' :: String -> ([Entry], String)
    parseEntries' "" = ([], "")
    parseEntries' (' ':r) = parseEntries' r
    parseEntries' (_:"") = ([], "")
    parseEntries' s@(c:r@(n:_)) | c `elem` ['A' .. 'Z'] ++ ['a' .. 'z'] &&
                                  n `elem` ['0' .. '9'] ++ ['-', '.'] = prependEntry c $ parseArgument r
                                | otherwise = ([], s)
    
    -- prependEntry :: Char -> (Value, String) -> ([Entry], String)
    prependEntry c (v, s) = resultEntry (Entry (toUpper c) v) (parseEntries' s)
    
    -- resultEntry :: Entry -> ([Entry], String) -> ([Entry], String)
    resultEntry a (e, s) = (a:e, s)

parseArgument :: String -> (Value, String)
parseArgument s = result $ parseDigits s
  where
    -- result :: (Value, String, Int, Int) -> (Value, String)
    result (v, s, _, _) = (v, s)
    
    -- parseDigits :: String -> (Value, String, Int, Int)
    parseDigits "" = (Value 0 0, "", 0, 1)
    parseDigits ('-':r) = negative $ parseDigits r
    parseDigits ('.':r) = setPoint $ parseDigits r
    parseDigits s@(d:r) | d `elem` "0123456789" = prependDigit d $ parseDigits r
                        | otherwise = (Value 0 0, s, 0, 1)
    
    -- prependDigit :: Char -> (Value, String, Int, Int) -> (Value, String, Int, Int)
    prependDigit d (v, s, p, e) = (v { intValue = e * charToDigit d + intValue v }, s, p + 1, e * 10)
    
    -- charToDigit :: Char -> Int
    charToDigit d = fromEnum d - fromEnum '0'
    
    -- setPoint :: (Value, String, Int, Int) -> (Value, String, Int, Int)
    setPoint (v, s, p, e) = (v { decPoint = p }, s, p, e)
    
    -- negative :: (Value, String, Int, Int) -> (Value, String, Int, Int)
    negative (v, s, p, e) = (v { intValue = -intValue v }, s, p, e)

formatCommand :: Command -> String
formatCommand c | command' /= "" && comment' /= "" = command' ++ " " ++ comment'
                | otherwise = command' ++ comment'
  where
    command' = formatInstruction' c
    comment' = formatComment' c
    formatInstruction' (Command i _) = formatInstruction i
    formatComment' (Command _ c) = formatComment c

formatComment :: String -> String
formatComment "" = ""
formatComment s = "; " ++ s

formatInstruction :: Instruction -> String
formatInstruction (Empty) = ""
formatInstruction (Entries e) = formatEntries' e
  where
    -- formatEntries :: [Entry] -> String
    formatEntries' [] = ""
    formatEntries' (e:r) = formatEntry e ++ formatEntries' r
formatInstruction (Directive k v) = k ++ "," ++ v

formatEntry :: Entry -> String
formatEntry (Entry k v) = k : formatValue v

formatValue :: Value -> String
formatValue (Value 0 _) = "0"
formatValue (Value value point) | value < 0 = '-' : (result $ formatDigits (-value) point 1 point)
                                | otherwise = result $ formatDigits value point 1 point
  where
    -- result :: (String, Int, Int) -> String
    result (s, _, _) = s
    
    -- formatDigits :: Int -> Int -> Int -> Int -> (String, Int, Int)
    formatDigits 0 0 _ _ = ("0", 0, 0)
    formatDigits d p e i | d >= e || i >= 0 = prependDigit p $ formatDigits d p (e * 10) (i - 1)
                       | otherwise = ("", d, 0)
    
    -- prependDigit :: Int -> (String, Int, Int) -> (String, Int, Int)
    prependDigit p (s, d, i) = (digitToChar (d `mod` e) : (prependPoint i p s), d `div` e, i + 1)
      where e = 10
    
    -- prependPoint :: Int -> Int -> String -> String
    prependPoint i p s | i == p && i > 0 = '.' : s
                       | otherwise = s
    
    -- digitToChar :: Int -> Char
    digitToChar d = toEnum (d + fromEnum '0')
