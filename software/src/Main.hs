module Main where

import Paths_gerber2reprap(version)
import Data.Version(showVersion)
--import Data.Maybe(fromMaybe)

import System.Console.GetOpt(getOpt, usageInfo, ArgOrder(Permute), ArgDescr(NoArg, ReqArg), OptDescr(Option))
import System.Environment(getArgs, getProgName)
import System.Exit(exitWith, ExitCode(ExitSuccess, ExitFailure))
import System.IO(openFile, hClose, IOMode(ReadMode, WriteMode), hPutStrLn, stdin, stdout, stderr)

import qualified Transform as T

main :: IO ()
main = getArgs >>= parseArgs >>= convertGCode

data Action = ShowUsage | ShowVersion | DoTransform
            deriving (Show, Eq)

data Options =
  Options
  { optAction     :: Action
  , optNoReset    :: Bool
  , optNoParking  :: Bool
  , optComments   :: Bool
  , optDrillDepth :: Float
  , optMoveSpeed  :: Float
  , optDrillSpeed :: Float
  , optGerberFile :: FilePath
  , optRepRapFile :: FilePath
  } deriving (Show, Eq)

-- defaults :: Options
defaults =
  Options
  { optAction     = DoTransform
  , optNoReset    = not $ T.needReset T.defaults
  , optNoParking  = not $ T.doParking T.defaults
  , optComments   = T.addComment T.defaults
  , optDrillDepth = T.valueTo $ T.drillDepth T.defaults
  , optMoveSpeed  = min2sec' $ T.valueTo $ T.moveSpeed T.defaults
  , optDrillSpeed = min2sec' $ T.valueTo $ T.drillSpeed T.defaults
  , optGerberFile = "-"
  , optRepRapFile = "-"
  }

-- options :: [OptDescr]
options =
  [ Option ['h','?'] ["help"]
    (NoArg (\opts -> opts { optAction = ShowUsage }))
    "This usage hint."
  , Option ['V']     ["version"]
    (NoArg (\opts -> opts { optAction = ShowVersion }))
    "Version information."
  , Option ['r']     ["dont-reset"]
    (NoArg (\opts -> opts { optNoReset = True }))
    "Do not reset coords before processing."
  , Option ['p']     ["dont-parking"]
    (NoArg (\opts -> opts { optNoParking = True }))
    "Do not park head for changing tool."
  , Option ['c']     ["add-comments"]
    (NoArg (\opts -> opts { optComments = True }))
    "Adds comments after each command."
  , Option ['d']     ["drill-depth"]
    (ReqArg (\val opts -> opts { optDrillDepth = read val }) "VALUE")
    ("Board depth in target units. (default: " ++ (show $ optDrillDepth defaults) ++ ")")
  , Option ['m']     ["move-speed"]
    (ReqArg (\val opts -> opts { optMoveSpeed = read val }) "VALUE")
    ("Moving speed in units/second. (default: " ++ (show $ optMoveSpeed defaults) ++ ")")
  , Option ['s']     ["drill-speed"]
    (ReqArg (\val opts -> opts { optDrillSpeed = read val }) "VALUE")
    ("Drilling speed in units/second. (default: " ++ (show $ optDrillSpeed defaults) ++ ")")
  , Option ['o']     ["output"]
    (ReqArg (\val opts -> opts { optRepRapFile = val }) "FILE")
    "Output GCode file."
  ]

-- parseArgs :: [String] -> IO Options
parseArgs argv = getProgName >>= parseArgs' argv

-- parseArgs' :: [String] -> String -> IO Options
parseArgs' argv program = case getOpt Permute options argv of
  (args,fs,[]) -> do
    doAction $ foldl (flip id) defaults $ concat [ args
                                                 , map (\val opts -> opts { optGerberFile = val }) fs
                                                 ]
  
  (_,_,errs) -> do
    hPutStrLn stderr (concat errs ++ usageInfo usage options)
    exitWith $ ExitFailure 1
 
  where
    -- usage :: String
    usage = "Usage: " ++ program ++ " [options] [excellon.drl]"
    
    -- verst :: String
    verst = program ++ " " ++ showVersion version
    
    -- doAction :: Options -> IO ()
    doAction opts = case optAction opts of 
      DoTransform -> return opts
      ShowUsage -> do
        hPutStrLn stderr (usageInfo usage options)
        exitWith ExitSuccess
      ShowVersion -> do
        hPutStrLn stderr verst
        exitWith ExitSuccess

-- convertGCode :: Options -> IO ()
convertGCode opts = do
--  putStrLn $ show opts
  gerberHandle <- open (optGerberFile opts) ReadMode stdin
  reprapHandle <- open (optRepRapFile opts) WriteMode stdout
  T.process (T.Params { T.needReset  = not $ optNoReset opts
                      , T.doParking  = not $ optNoParking opts
                      , T.addComment = optComments opts
                      , T.drillSpeed = T.valueFrom 3 $ sec2min' $ optDrillSpeed opts
                      , T.drillDepth = T.valueFrom 3 $ optDrillDepth opts
                      , T.moveSpeed  = T.valueFrom 3 $ sec2min' $ optMoveSpeed opts
                      , T.coordsType = T.Absolute
                      , T.unitsType  = T.Millimeters
                      }) gerberHandle reprapHandle
  close (optGerberFile opts) gerberHandle
  close (optRepRapFile opts) reprapHandle
  where open file mode stdx = if file == "-" then return stdx else openFile file mode
        close file handle = if file == "-" then return () else hClose handle

min2sec' val = val / 60
sec2min' val = val * 60
