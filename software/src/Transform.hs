module Transform ( Params(Params)
                 , needReset
                 , doParking
                 , addComment
                 , drillSpeed
                 , drillDepth
                 , moveSpeed
                 , coordsType
                 , Coords(Absolute, Relative)
                 , unitsType
                 , Units(Millimeters, Inches)
                 , defaults
                 , process
                   --, prepare
                   --, initialize
                   --, finalize
                   --, transform 
                 , valueFrom
                 , valueTo
                 ) where

import System.IO(Handle, hGetLine, hPutStrLn, hIsEOF)
import GCode

data Coords = Absolute | Relative
            deriving (Show, Eq)

data Units = Millimeters | Inches
           deriving (Eq)

instance Show Units where
  show Millimeters = "mm"
  show Inches = "in"

data Params =
  Params
  { needReset  :: Bool
  , doParking  :: Bool
  , addComment :: Bool
  , moveSpeed  :: Value
  , drillSpeed :: Value
  , drillDepth :: Value
  , coordsType :: Coords
  , unitsType  :: Units
  }

-- defaults :: Params
defaults =
  Params
  { needReset  = True
  , doParking  = True
  , addComment = False
  , moveSpeed  = Value 1500 0 -- units/min
  , drillSpeed = Value 18 0 -- units/min
  , drillDepth = Value 3 0  -- units
  , coordsType = Absolute
  , unitsType  = Millimeters
  }

process :: Params -> Handle -> Handle -> IO ()
process params input output = initialize' >> process' params >> finalize'
  where
    initialize' :: IO ()
    initialize' = outputCommands $ initialize params
    
    finalize' :: IO ()
    finalize' = outputCommands $ finalize params
    
    process' :: Params -> IO ()
    process' params = do
      eof <- hIsEOF input
      if eof then return () else do
        line <- hGetLine input
        -- process
        let command = parse line
            params' = prepare params command
            commands' = transform params' command
        --hPutStrLn output $ show $ cmd
        outputCommands $ commands'
        --hPutStrLn output line
        process' params'
    
    outputCommands :: [Command] -> IO ()
    outputCommands [] = return ()
    outputCommands (cmd:rest) = do
      hPutStrLn output $ format cmd
      outputCommands rest

initialize :: Params -> [Command]
initialize p = resetCoords p

finalize :: Params -> [Command]
finalize _ = []

prepare :: Params -> Command -> Params
prepare p (Command (Entries (Entry 'G' (Value 90 0) : _)) _) = p { coordsType = Absolute }
prepare p (Command (Entries (Entry 'G' (Value 91 0) : _)) _) = p { coordsType = Relative }
prepare p (Command (Entries (Entry 'G' (Value 20 0) : _)) _) = p { unitsType = Inches }
prepare p (Command (Entries (Entry 'G' (Value 21 0) : _)) _) = p { unitsType = Millimeters }
prepare p (Command (Directive "METRIC" _) _) = p { unitsType = Millimeters }
prepare p (Command (Directive "INCH" _) _) = p { unitsType = Inches }
prepare p _ = p

transform :: Params -> Command -> [Command]
transform p (Command (Entries (Entry 'T' t : Entry 'C' c : _)) _) = setupTool p t c
transform p (Command (Entries (Entry 'T' t : _)) _) = parkHead p ++ changeTool p t
transform p (Command (Entries (Entry 'X' x : Entry 'Y' y : _)) _) = drillHole p x y
transform p (Command (Entries (Entry 'Y' y : Entry 'X' x : _)) _) = drillHole p x y
transform p (Command (Entries (Entry 'G' (Value 90 0) : _)) _) = [ Command
                                                                   (Entries
                                                                    [ Entry 'G' $ Value 90 0
                                                                    ]) $ comment p "Set absolute positioning"
                                                                 ]
transform p (Command (Entries (Entry 'G' (Value 91 0) : _)) _) = [ Command
                                                                   (Entries
                                                                    [ Entry 'G' $ Value 90 0
                                                                    ]) $ comment p "Set relative positioning"
                                                                 ]
transform p (Command (Directive "METRIC" _) _) = [ Command
                                                   (Entries
                                                    [ Entry 'G' $ Value 21 0
                                                    ]) $ comment p "Set units to millimeters"
                                                 ]
transform p (Command (Directive "INCH" _) _) = [ Command
                                                 (Entries
                                                  [ Entry 'G' $ Value 20 0
                                                  ]) $ comment p "Set units to inches"
                                               ]
transform _ _ = []

--comment :: Params -> String -> String
comment p s | addComment p = s
            | otherwise = ""

setupTool p t c = [ Command Empty ("Tool #" ++ formatValue t ++ ": " ++ formatValue c ++ " " ++ show (unitsType p)) ]

changeTool p (Value t _) | t /= 0 = [ Command
                                      (Entries
                                       [ Entry 'M' $ Value 300 0
                                       , Entry 'S' $ Value 4400 0
                                       , Entry 'P' $ Value 1600 0
                                       ]) $ comment p "Beep to change tool"
                                    , Command
                                      (Entries
                                       [ Entry 'M' $ Value 226 0
                                       ]) $ comment p "Pauses processing"
                                    ]
                         | otherwise = []

value0 = Value 0 0

entryG = Entry 'G'
entryG1 = entryG $ Value 1 0 -- Controlled movement
entryG92 = entryG $ Value 92 0 -- Set coords

entryX = Entry 'X'
entryY = Entry 'Y'
entryZ = Entry 'Z'

entryF = Entry 'F'  -- Speed
entryFMove = entryF . moveSpeed   -- Move speed
entryFDrill = entryF . drillSpeed -- Drill speed

parkHead p | doParking p = [ Command
                             (Entries
                              [ entryG1
                              , entryX value0
                              , entryY value0
                              , entryFMove p
                              ]) $ comment p "Tool change parking"
                           ]
           | otherwise = []

resetCoords :: Params -> [Command]
resetCoords p | needReset p = [ Command
                                (Entries
                                 [ entryG92
                                 , entryX $ value0
                                 , entryY $ value0
                                 , entryZ $ drillDepth p
                                 ]) $ comment p "Reset coords to initial point"
                              ]
              | otherwise = []

drillHole p x y = [ Command
                    (Entries
                     [ entryG1
                     , entryX x
                     , entryY y
                     , entryFMove p
                     ]) $ comment p "Moving to hole position"
                  , Command
                    (Entries
                     [ entryG1
                     , entryZ (if coordsType p == Absolute then value0 else negative $ drillDepth p)
                     , entryFDrill p
                     ]) $ comment p "Drilling hole down"
                  , Command
                    (Entries
                     [ entryG1
                     , entryZ $ drillDepth p
                     , entryFMove p
                     ]) $ comment p "Returning up"
                  ]
  where
    negative (Value d p) = Value (-d) p
